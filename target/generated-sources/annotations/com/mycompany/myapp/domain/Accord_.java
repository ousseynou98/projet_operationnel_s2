package com.mycompany.myapp.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Accord.class)
public abstract class Accord_ {

	public static volatile SingularAttribute<Accord, LocalDate> date;
	public static volatile SingularAttribute<Accord, String> description;
	public static volatile SingularAttribute<Accord, Long> id;
	public static volatile SingularAttribute<Accord, String> structure;

	public static final String DATE = "date";
	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String STRUCTURE = "structure";

}

