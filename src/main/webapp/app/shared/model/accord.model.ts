import { Moment } from 'moment';

export interface IAccord {
  id?: number;
  structure?: string;
  description?: string;
  date?: Moment;
}

export class Accord implements IAccord {
  constructor(public id?: number, public structure?: string, public description?: string, public date?: Moment) {}
}
