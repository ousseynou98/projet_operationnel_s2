import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAccord, Accord } from 'app/shared/model/accord.model';
import { AccordService } from './accord.service';

@Component({
  selector: 'jhi-accord-update',
  templateUrl: './accord-update.component.html',
})
export class AccordUpdateComponent implements OnInit {
  isSaving = false;
  dateDp: any;

  editForm = this.fb.group({
    id: [],
    structure: [],
    description: [],
    date: [],
  });

  constructor(protected accordService: AccordService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ accord }) => {
      this.updateForm(accord);
    });
  }

  updateForm(accord: IAccord): void {
    this.editForm.patchValue({
      id: accord.id,
      structure: accord.structure,
      description: accord.description,
      date: accord.date,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const accord = this.createFromForm();
    if (accord.id !== undefined) {
      this.subscribeToSaveResponse(this.accordService.update(accord));
    } else {
      this.subscribeToSaveResponse(this.accordService.create(accord));
    }
  }

  private createFromForm(): IAccord {
    return {
      ...new Accord(),
      id: this.editForm.get(['id'])!.value,
      structure: this.editForm.get(['structure'])!.value,
      description: this.editForm.get(['description'])!.value,
      date: this.editForm.get(['date'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAccord>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
