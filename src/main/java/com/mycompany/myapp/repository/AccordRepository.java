package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Accord;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Accord entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccordRepository extends JpaRepository<Accord, Long> {
}
