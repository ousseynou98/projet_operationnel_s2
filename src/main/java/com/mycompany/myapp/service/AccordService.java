package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Accord;
import com.mycompany.myapp.repository.AccordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Accord}.
 */
@Service
@Transactional
public class AccordService {

    private final Logger log = LoggerFactory.getLogger(AccordService.class);

    private final AccordRepository accordRepository;

    public AccordService(AccordRepository accordRepository) {
        this.accordRepository = accordRepository;
    }

    /**
     * Save a accord.
     *
     * @param accord the entity to save.
     * @return the persisted entity.
     */
    public Accord save(Accord accord) {
        log.debug("Request to save Accord : {}", accord);
        return accordRepository.save(accord);
    }

    /**
     * Get all the accords.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Accord> findAll(Pageable pageable) {
        log.debug("Request to get all Accords");
        return accordRepository.findAll(pageable);
    }


    /**
     * Get one accord by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Accord> findOne(Long id) {
        log.debug("Request to get Accord : {}", id);
        return accordRepository.findById(id);
    }

    /**
     * Delete the accord by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Accord : {}", id);
        accordRepository.deleteById(id);
    }
}
